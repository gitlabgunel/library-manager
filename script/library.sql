INSERT INTO `library-manager`.`authority` (`name`) VALUES ('USER');
INSERT INTO `library-manager`.`user_type` (`name`) VALUES ('USER');
INSERT INTO `library-manager`.`user_type` (`name`) VALUES ('PUBLISHER');

INSERT INTO `library-manager`.`authors` (`age`, `name`) VALUES ('25', 'Author1');
INSERT INTO `library-manager`.`authors` (`age`, `name`) VALUES ('26', 'Author2');
INSERT INTO `library-manager`.`authors` (`age`, `name`) VALUES ('56', 'Author3');

package az.ingress.common.security.repository;

import az.ingress.common.security.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CommonUserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

}

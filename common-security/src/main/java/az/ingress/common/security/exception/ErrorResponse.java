package az.ingress.common.security.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
@RequiredArgsConstructor
@Data
public class ErrorResponse {
    private final String errorCode;
    private final String errorDescription;
}

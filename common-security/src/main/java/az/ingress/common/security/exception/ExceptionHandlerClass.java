package az.ingress.common.security.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolationException;
import java.util.Objects;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandlerClass {

    @ExceptionHandler({RestErrorResponseException.class})
    public ResponseEntity<ErrorResponse> handleException(RestErrorResponseException restErrorResponseException, WebRequest webRequest) {
        log.error(restErrorResponseException.getMessage(), restErrorResponseException);

            return ResponseEntity.status(restErrorResponseException.getHttpStatus()).body(restErrorResponseException.getErrorResponse());

    }
    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<Object> handleConstraintViolationException(Exception ex, WebRequest request) {
        log.error(ex.getMessage(), ex);
        ErrorResponse responseDTO = new ErrorResponse("400", ex.getMessage());
        return ResponseEntity.badRequest().body(responseDTO);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, WebRequest webRequest) {
        log.error(ex.getMessage(), ex);
        ObjectError error = ex.getBindingResult().getAllErrors().stream().findAny().orElse(null);
        FieldError fieldError = (FieldError) error;
        String errorMessage = String.format("%s- %s!",
                Objects.requireNonNull(fieldError).getField(), fieldError.getDefaultMessage());
        ErrorResponse errorResponse = new ErrorResponse("400", errorMessage);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }



}


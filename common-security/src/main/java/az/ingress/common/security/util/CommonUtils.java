package az.ingress.common.security.util;

import az.ingress.common.security.exception.ErrorResponse;
import az.ingress.common.security.exception.RestErrorResponseException;
import org.springframework.http.HttpStatus;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */
public class CommonUtils {
    private CommonUtils() {
    }

    public static final String AUTH_HEADER = "Authorization";
    public static final String BEARER = "Bearer";
    public static final String ROLES_CLAIM = "ROLE";

    public static String removeHeader(String header){
        if (header.startsWith("Bearer"))
            return header.substring(BEARER.length()+1);
        else
            throw new RestErrorResponseException(HttpStatus.BAD_REQUEST,new ErrorResponse("405","It is not Bearer token"));
    }
}

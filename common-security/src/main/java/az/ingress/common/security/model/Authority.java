package az.ingress.common.security.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "authority")
public class Authority {

    @Id
    @NotNull
    @Size(max = 100)
    @Column(length = 100)
    private String name;

}



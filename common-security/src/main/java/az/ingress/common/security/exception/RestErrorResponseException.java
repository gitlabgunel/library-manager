package az.ingress.common.security.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
@RequiredArgsConstructor
@Getter
@Setter
public class RestErrorResponseException extends RuntimeException {
 private final HttpStatus httpStatus;
 private final ErrorResponse errorResponse;
}
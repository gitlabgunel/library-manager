package az.ingress.user.management.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpHeaders;

@Data
@Builder
@AllArgsConstructor
public class AuthResponseDto {
    private HttpHeaders httpHeaders;
    private AccessTokenDto accessToken;
}

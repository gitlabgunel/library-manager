package az.ingress.user.management.config;

import az.ingress.common.security.auth.filter.JwtAuthFilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthFilterConfigurerAdapter filterConfigurerAdapter;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.apply(filterConfigurerAdapter);
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().authorizeRequests()
                .antMatchers("/users/sign-in")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/users").hasAnyAuthority("USER"); //ROLE_

        super.configure(http);
    }

}

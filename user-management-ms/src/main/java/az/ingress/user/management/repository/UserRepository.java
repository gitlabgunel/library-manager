package az.ingress.user.management.repository;

import az.ingress.common.security.model.User;
import az.ingress.common.security.repository.CommonUserRepository;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CommonUserRepository {

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findByEmail(String email);

    boolean existsByEmail(String email);

}

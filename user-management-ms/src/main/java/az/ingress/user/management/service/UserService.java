package az.ingress.user.management.service;

import az.ingress.user.management.dto.AuthResponseDto;
import az.ingress.user.management.dto.LoginDto;
import az.ingress.user.management.dto.SignUpDto;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
public interface UserService {
    AuthResponseDto login(LoginDto loginDto);
    void signUp(SignUpDto signUpDto);
}

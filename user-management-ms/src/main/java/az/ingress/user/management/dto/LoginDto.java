package az.ingress.user.management.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */
@Data
public class LoginDto {
    @NotNull
    @Size(min = 1, max = 50)
    private String email;

    @NotNull
    private String password;
}

package az.ingress.user.management.enumeration;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */
public enum UserTypeEnum {
    USER(1),
    PUBLISHER(2);

    private final long id;

    UserTypeEnum(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}

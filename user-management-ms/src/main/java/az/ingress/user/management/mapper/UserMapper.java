package az.ingress.user.management.mapper;

import az.ingress.common.security.model.User;
import az.ingress.user.management.dto.SignUpDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;


/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User toUserEntity(SignUpDto signUpDto);
}

package az.ingress.user.management.controller;

import az.ingress.user.management.dto.AccessTokenDto;
import az.ingress.user.management.dto.AuthResponseDto;
import az.ingress.user.management.dto.LoginDto;
import az.ingress.user.management.dto.SignUpDto;
import az.ingress.user.management.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @PostMapping("/sign-in")
    public ResponseEntity<AccessTokenDto> authorize(@Valid @RequestBody LoginDto loginDto) {
        AuthResponseDto login = userService.login(loginDto);
        return new ResponseEntity<>(login.getAccessToken(), login.getHttpHeaders(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity signUp(@RequestBody @Valid SignUpDto signUpDto) {
        userService.signUp(signUpDto);
        return ResponseEntity.noContent().build();
    }
}

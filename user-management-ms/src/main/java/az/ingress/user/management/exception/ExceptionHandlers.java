package az.ingress.user.management.exception;

import az.ingress.common.security.exception.ErrorResponse;
import az.ingress.common.security.exception.ExceptionHandlerClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.validation.ConstraintViolationException;
import java.util.Objects;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandlers extends ExceptionHandlerClass {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Object> handleNotReadable(HttpMessageNotReadableException ex) {
        log.error(ex.getMessage(), ex);
        ErrorResponse errorResponse=new ErrorResponse(String.valueOf(HttpStatus.BAD_REQUEST.value()),
                ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

}


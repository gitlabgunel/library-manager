package az.ingress.user.management;

import az.ingress.common.security.auth.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = {"az.ingress.common.security", "az.ingress.user.management"})
@EntityScan({"az.ingress.common.security.model", "az.ingress.book.management"})
@EnableJpaRepositories({"az.ingress.common.security", "az.ingress.user.management"})
@SpringBootApplication
public class UserManagementMsApplication {


    public static void main(String[] args) {
        SpringApplication.run(UserManagementMsApplication.class, args);
    }

}

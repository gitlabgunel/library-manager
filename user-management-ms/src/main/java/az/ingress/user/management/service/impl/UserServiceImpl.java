package az.ingress.user.management.service.impl;

import az.ingress.common.security.auth.jwt.JwtService;
import az.ingress.common.security.exception.ErrorResponse;
import az.ingress.common.security.exception.RestErrorResponseException;
import az.ingress.common.security.model.Authority;
import az.ingress.common.security.model.User;
import az.ingress.common.security.model.UserType;
import az.ingress.common.security.model.VerificationToken;
import az.ingress.user.management.dto.AccessTokenDto;
import az.ingress.user.management.dto.AuthResponseDto;
import az.ingress.user.management.dto.LoginDto;
import az.ingress.user.management.dto.SignUpDto;
import az.ingress.user.management.mapper.UserMapper;
import az.ingress.user.management.repository.UserRepository;
import az.ingress.user.management.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;


    @Override
    public AuthResponseDto login(LoginDto loginDto) {
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getEmail(),
                loginDto.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        Duration duration = Duration.ofDays(7);
        String jwt = jwtService.issueToken(authentication, duration);
        System.out.println(jwt);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return AuthResponseDto.builder().accessToken(new AccessTokenDto(jwt)).httpHeaders(httpHeaders).build();
    }

    @Override
    public void signUp(SignUpDto signUpDto) {
        if (userRepository.existsByEmail(signUpDto.getEmail()))
            throw new RestErrorResponseException(HttpStatus.BAD_REQUEST, new ErrorResponse(HttpStatus.BAD_REQUEST.name(), "User already exists"));
        User user = UserMapper.INSTANCE.toUserEntity(signUpDto);
        VerificationToken verificationToken=VerificationToken
                .builder()
                .token(UUID.randomUUID().toString())
                .build();
        user.setVerificationToken(verificationToken);
        Set<Authority> authorities = new HashSet<>();
        authorities.add(new Authority("USER"));
        user.setAuthorities(authorities);
        user.setUserType(new UserType(signUpDto.getUserType().getId()));
        verificationToken.setUser(user);
        userRepository.save(user);
    }
}

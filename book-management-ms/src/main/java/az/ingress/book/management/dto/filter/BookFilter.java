package az.ingress.book.management.dto.filter;

import lombok.Data;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */

@Data
public class BookFilter {
    private String id;
    private String name;
    private String authorName;
}

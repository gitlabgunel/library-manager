package az.ingress.book.management.dto;

import lombok.Data;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */
@Data
public class BookRequestDto {
    private String name;
    private String description;
    private Long authorId;
    private Long publisherId;
}

package az.ingress.book.management.service.impl;

import az.ingress.book.management.domain.Author;
import az.ingress.book.management.domain.Book;
import az.ingress.book.management.dto.BookRequestDto;
import az.ingress.book.management.dto.BookResponseDto;
import az.ingress.book.management.dto.filter.BookFilter;
import az.ingress.book.management.mapper.BookMapper;
import az.ingress.book.management.repository.BookRepository;
import az.ingress.book.management.service.BookService;
import az.ingress.book.management.service.BookValidationService;
import az.ingress.book.management.specification.BookSpecification;
import az.ingress.common.security.exception.ErrorResponse;
import az.ingress.common.security.exception.RestErrorResponseException;
import az.ingress.common.security.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final BookValidationService bookValidationService;

    @Override
    public Page<BookResponseDto> getAllBooks(Pageable pageable, BookFilter bookFilter) {
        Page<Book> result = bookRepository.findAll(new BookSpecification(bookFilter), pageable);
        return new PageImpl<BookResponseDto>(BookMapper.INSTANCE.toResponseDtoListFromPage(result), pageable, result.getTotalElements());
    }

    @Override
    public void addBook(BookRequestDto bookRequestDto) {
        bookValidationService.checkIsValidBook(bookRequestDto);
        Book book = BookMapper.INSTANCE.toBookEntity(bookRequestDto);
        bookRepository.save(book);
    }

    @Override
    public List<BookResponseDto> getBooksByPublisher(Long id) {
        List<Book> bookList = bookRepository.findAllByByPublisherId(id);
        return BookMapper.INSTANCE.toResponseDtoList(bookList);
    }

    @Override
    public void update(Long id, BookRequestDto bookRequestDto) {
        bookValidationService.checkIsValidBook(bookRequestDto);
        Book book = bookRepository.findById(id).orElseThrow(() -> new RestErrorResponseException(HttpStatus.NOT_FOUND,
                new ErrorResponse("404", "Not found book by id")));
        book.setAuthor(Author.builder().id(bookRequestDto.getAuthorId()).build());
        book.setByPublisher(User.builder().id(bookRequestDto.getPublisherId()).build());
        book.setDescription(bookRequestDto.getDescription());
        book.setName(bookRequestDto.getName());
        bookRepository.save(book);
    }

    @Override
    public boolean checkIfBookCreatedByAuthUser(Long id, Object principal) {
        log.info("Id {} Username {}",id,principal);
        String email=bookRepository.findById(id).get().getByPublisher().getEmail();
        return email.equals(principal)?true:false;
    }
}

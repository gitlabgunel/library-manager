package az.ingress.book.management.specification;

import az.ingress.book.management.domain.Book;
import az.ingress.book.management.dto.filter.BookFilter;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gunel Mammadli
 * @date 02-Feb-22
 */
@AllArgsConstructor
public class BookSpecification implements Specification<Book> {
    private BookFilter bookFilter;


    @Override
    public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();
        if (bookFilter.getId() != null)
            predicates.add(criteriaBuilder.equal(root.get("id"), bookFilter.getId()));
        if (bookFilter.getName() != null)
            predicates.add(criteriaBuilder.equal(root.get("name"), bookFilter.getName()));
        if (bookFilter.getAuthorName() != null)
            predicates.add(criteriaBuilder.equal(root.get("author").get("name"), bookFilter.getAuthorName()));
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}

package az.ingress.book.management.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */

@Data
@Builder
public class AuthorResponseDto {
    private Long id;
    private String name;
    private int age;
}

package az.ingress.book.management.config;

import az.ingress.common.security.auth.filter.JwtAuthFilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthFilterConfigurerAdapter filterConfigurerAdapter;


    @Value("${security.basic.auth.username}")
    private String username;
    @Value("${security.basic.auth.password}")
    private String password;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.apply(filterConfigurerAdapter);
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/books/publisher/**")
                .hasAnyAuthority( "USER")
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/books**").hasAnyAuthority("USER", "PUBLISHER")
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.PUT,"/books/**").hasAnyAuthority("PUBLISHER")
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/books").hasAnyAuthority( "PUBLISHER");

        super.configure(http);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication()
                .withUser(username)
                .password("{noop}" + password)
                .roles("USER");
    }
}

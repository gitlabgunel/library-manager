package az.ingress.book.management.repository;

import az.ingress.book.management.domain.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {
    @EntityGraph(attributePaths = {"author"})
    Page<Book> findAll(Specification specification,Pageable pageable);

    @EntityGraph(attributePaths = {"author"})
    List<Book> findAllByByPublisherId(Long publisherId);

    Optional<Book> findById(Long id);
}

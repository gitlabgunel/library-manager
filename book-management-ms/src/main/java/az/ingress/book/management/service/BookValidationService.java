package az.ingress.book.management.service;

import az.ingress.book.management.dto.BookRequestDto;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */
public interface BookValidationService {

    void checkIsValidBook(BookRequestDto bookRequestDto);
}

package az.ingress.book.management.mapper;

import az.ingress.book.management.domain.Book;
import az.ingress.book.management.dto.BookRequestDto;
import az.ingress.book.management.dto.BookResponseDto;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;


/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
//@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
@Mapper
public interface BookMapper {
    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    BookResponseDto toResponseDto(Book book);

     List<BookResponseDto> toResponseDtoList(List<Book> books);

    List<BookResponseDto> toResponseDtoListFromPage(Page<Book> books);

    @Mapping(source = "publisherId", target = "byPublisher.id")
    @Mapping(source = "authorId", target = "author.id")
    Book toBookEntity(BookRequestDto bookRequestDto);
//    @AfterMapping
//    default void afterMapping(@MappingTarget BookResponseDto responseDto,Book book) {
//
//    }
}

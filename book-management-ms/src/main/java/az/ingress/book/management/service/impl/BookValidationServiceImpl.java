package az.ingress.book.management.service.impl;

import az.ingress.book.management.dto.BookRequestDto;
import az.ingress.book.management.repository.AuthorRepository;
import az.ingress.book.management.service.BookValidationService;
import az.ingress.common.security.exception.ErrorResponse;
import az.ingress.common.security.exception.RestErrorResponseException;
import az.ingress.common.security.repository.CommonUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * @author Gunel Mammadli
 * @date 30-Jan-22
 */

@RequiredArgsConstructor
@Service
public class BookValidationServiceImpl implements BookValidationService {
    private final AuthorRepository authorRepository;
    private final CommonUserRepository userRepository;

    @Override
    public void checkIsValidBook(BookRequestDto bookRequestDto) {
        userRepository.findById(bookRequestDto.getPublisherId()).orElseThrow(() ->
                new RestErrorResponseException(HttpStatus.NOT_FOUND, new ErrorResponse("404", "Not found publisher")));
        authorRepository.findById(bookRequestDto.getAuthorId()).orElseThrow(() ->
                new RestErrorResponseException(HttpStatus.NOT_FOUND, new ErrorResponse("404", "Not found author")));
    }


}

package az.ingress.book.management.controller;

import az.ingress.book.management.dto.BookRequestDto;
import az.ingress.book.management.dto.BookResponseDto;
import az.ingress.book.management.dto.filter.BookFilter;
import az.ingress.book.management.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    @GetMapping
    public Page<BookResponseDto> getBookList(Pageable pageable, BookFilter bookFilter) {
        return bookService.getAllBooks(pageable, bookFilter);
    }

    @GetMapping("/publisher/{id}")
    public ResponseEntity getBookListByPublisher(@PathVariable Long id) {
        return  ResponseEntity.ok(bookService.getBooksByPublisher(id));
    }

    @PostMapping
    public ResponseEntity addBook(@RequestBody BookRequestDto bookRequestDto) {
        bookService.addBook(bookRequestDto);
        return ResponseEntity.noContent().build();
    }


    @PreAuthorize("@bookServiceImpl.checkIfBookCreatedByAuthUser(#id,authentication.principal)")
    @PutMapping("/{id}")
    public ResponseEntity updateBook(@PathVariable Long id, @RequestBody BookRequestDto bookRequestDto) {
        bookService.update(id, bookRequestDto);
        return ResponseEntity.noContent().build();
    }
}

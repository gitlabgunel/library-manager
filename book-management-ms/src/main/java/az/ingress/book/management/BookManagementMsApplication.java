package az.ingress.book.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = {"az.ingress.common.security","az.ingress.book.management"})
@EntityScan({"az.ingress.common.security.model","az.ingress.book.management"})
@EnableJpaRepositories({"az.ingress.common.security", "az.ingress.book.management"})
@SpringBootApplication
public class BookManagementMsApplication {

    public static void main(String[] args) {

        SpringApplication.run(BookManagementMsApplication.class, args);
    }

}

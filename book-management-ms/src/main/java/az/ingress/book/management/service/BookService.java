package az.ingress.book.management.service;

import az.ingress.book.management.dto.BookRequestDto;
import az.ingress.book.management.dto.BookResponseDto;
import az.ingress.book.management.dto.filter.BookFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;

/**
 * @author Gunel Mammadli
 * @date 29-Jan-22
 */
public interface BookService {
    Page<BookResponseDto> getAllBooks(Pageable pageable, BookFilter bookFilter);
    void addBook(BookRequestDto bookRequestDto);

    List<BookResponseDto>getBooksByPublisher(Long id);

    void update(Long id, BookRequestDto bookRequestDto);
    boolean checkIfBookCreatedByAuthUser(Long id, Object principal);
}

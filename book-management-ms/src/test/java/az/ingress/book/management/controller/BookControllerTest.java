package az.ingress.book.management.controller;

import az.ingress.book.management.domain.Author;
import az.ingress.book.management.domain.Book;
import az.ingress.book.management.dto.AuthorResponseDto;
import az.ingress.book.management.dto.BookResponseDto;
import az.ingress.book.management.dto.filter.BookFilter;
import az.ingress.book.management.service.BookService;
import az.ingress.book.management.service.BookValidationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Gunel Mammadli
 * @date 02-Feb-22
 */

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(BookController.class)
//@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
class BookControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private BookValidationService bookValidationService;

    @MockBean
    private BookService bookService;

    private static final String BASE_PATH = "/v1/books";

//    @Test
    void givenValidInputWhenGetBooksThenOk() throws Exception {
        //Arrange
        final Page<BookResponseDto> responseDto = Page.empty();
        when(bookService.getAllBooks( any(Pageable.class),any(BookFilter.class))).thenReturn(responseDto);

        //Act && Assert
        mockMvc.perform(get(BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


}
package az.ingress.book.management.service.impl;

import az.ingress.book.management.domain.Author;
import az.ingress.book.management.domain.Book;
import az.ingress.book.management.dto.AuthorResponseDto;
import az.ingress.book.management.dto.BookResponseDto;
import az.ingress.book.management.dto.filter.BookFilter;
import az.ingress.book.management.mapper.BookMapper;
import az.ingress.book.management.mapper.BookMapperImpl;
import az.ingress.book.management.repository.BookRepository;
import az.ingress.book.management.specification.BookSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.util.AssertionErrors;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * @author Gunel Mammadli
 * @date 31-Jan-22
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@SpringBootTest(classes = {BookMapperImpl.class})
class BookServiceImplTest {
    private Predicate predicate;
    @Mock
    private BookRepository bookRepository;
    @InjectMocks
    private BookValidationServiceImpl bookValidationService;
    @Autowired
    private BookMapper bookMapper;
    @InjectMocks
    BookServiceImpl bookService;


    @BeforeEach
    public void before() {
//        bookService = new BookServiceImpl(bookRepository,bookValidationService);
    }

//    @Test
    public void whenGetBooksByPublisherThenReturnList() {
        bookService.getBooksByPublisher(any());
        verify(bookRepository, times(1)).findAllByByPublisherId(any());
    }

//    @Test
    void givenSearchParamsWhenSearchThenResult() {
        // Arrange
        Book book = Book.builder().id(1L).author(Author.builder().id(1L).name("Author name").age(25).build())
                .build();
        BookResponseDto bookResponseDto = BookResponseDto.builder()
                .name("Book Name")
                .description("Book Description")
                .author(AuthorResponseDto.builder().id(1L).name("Author name").age(25).build())
                .build();
        List<BookResponseDto> listBooks = Arrays.asList(bookResponseDto);

        when(bookRepository
                .findAll(any(BookSpecification.class), any(Pageable.class)))
                .thenReturn(new PageImpl<Book>(List.of(book)));


        bookService.getAllBooks(any(Pageable.class), any(BookFilter.class));

        verify(bookRepository, times(1))
                .findAll(any(Specification.class), any(Pageable.class));
    }
}